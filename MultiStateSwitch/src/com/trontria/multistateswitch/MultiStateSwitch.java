package com.trontria.multistateswitch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class MultiStateSwitch extends View {
	public static final String TAG = MultiStateSwitch.class.getSimpleName();
	public static final int DEFAULT_STATE_COUNT = 3;
	
	private int mWidth;
	private int mHeight;
	private Bitmap mSwitchBackground;
	private Bitmap mDial;
	private int mState;
	private int mStateCount = DEFAULT_STATE_COUNT;
	private Paint mDrawPaint;
	private Rect mRectDest, mRectDestDial, mRectSrcBg, mRectSrcDial;

	public MultiStateSwitch(Context context) {
		super(context);
		init();
	}
	public MultiStateSwitch(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	public MultiStateSwitch(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}
	
	private void init() {
		mDrawPaint = new Paint();
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		Log.d(TAG, "onSizeChanged");

		mWidth = w;
		mHeight = h;
		mRectDest = new Rect(0, 0, mWidth, mHeight);
		
		// Reset default state
		setState(getState());
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Log.d(TAG, "onDraw");
		canvas.drawBitmap(mSwitchBackground, mRectSrcBg, mRectDest, mDrawPaint);
		if (mRectDestDial != null) {
			Log.e(TAG, "onDraw Dial " + mRectDestDial.left + " " + mRectDestDial.top + " "  + mRectDestDial.right + " " + mRectDestDial.bottom);
			canvas.drawBitmap(mDial, mRectSrcDial, mRectDestDial, mDrawPaint);
		}
	}
	
	private void onDataChanged() {
		Log.d(TAG, "onDataChanged");
		int smallWidth = mWidth;
		int smallHeight = mHeight / getStateCount();
		
		int positionY = getState() * mHeight / getStateCount();
		mRectDestDial = new Rect(0, positionY, smallWidth, positionY + smallHeight);
		dialStartY = positionY;
	}
	/**
	 * @return the state
	 */
	public int getState() {
		return mState;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		setState(state, false);
	}
	
	public void setState(int state, boolean fromUser) {
		mState = state;
		onDataChanged();
		invalidate();
		if (getOnSwitchStateChangedListener() != null) {
			getOnSwitchStateChangedListener().onStateChanged(state, fromUser);
		}
	}
	/**
	 * @return the switchBackground
	 */
	public Bitmap getSwitchBackground() {
		return mSwitchBackground;
	}
	/**
	 * @param switchBackground the switchBackground to set
	 */
	public void setSwitchBackground(Bitmap switchBackground) {
		mRectSrcBg = new Rect(0, 0, switchBackground.getWidth(), switchBackground.getHeight());
		mSwitchBackground = switchBackground;
		invalidate();
	}
	/**
	 * @return the dial
	 */
	public Bitmap getDial() {
		return mDial;
	}
	
	/**
	 * @param dial the dial to set
	 */
	public void setDial(Bitmap dial) {
		mRectSrcDial = new Rect(0, 0, dial.getWidth(), dial.getHeight());
		mDial = dial;
		invalidate();
	}
	
	/**
	 * @return the stateCount
	 */
	public int getStateCount() {
		return mStateCount;
	}
	/**
	 * @param stateCount the stateCount to set
	 */
	public void setStateCount(int stateCount) {
		mStateCount = stateCount;
		onDataChanged();
		invalidate();
	}
	
	private float startY = 0;
	private float dialStartY;
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float distance = event.getY() - startY;
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			Log.d(TAG, "ACTION_DOWN");
			startY = event.getY();
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			//
			Log.d(TAG, "ACTION_UP");
			float lastY = dialStartY + distance;
			
			int stepHeight = mHeight / mStateCount;
			if (lastY < 0) {
				setState(0);
			} else if (lastY > (mStateCount - 1 + 0.5) * stepHeight) {
				setState(mStateCount - 1);
			} else {
				for (int i = 0; i < mStateCount; i++) {
					if (Math.abs(lastY - i * stepHeight) < stepHeight / 2) {
						Log.d(TAG, "ACTION_UP found state " + i);
						setState(i, true);
						break;
					}
				}
			}
			break;
		case MotionEvent.ACTION_MOVE:
			Log.d(TAG, "ACTION_MOVE " + event.getY() + " " + startY);
			onDialMove(distance);
			invalidate();
			break;
		default:
			break;
		}
		return true;
	}
	
	private void onDialMove(float distance) {
		Log.d(TAG, "onDialMove(" + distance + ")");
		int smallWidth = mWidth;
		int smallHeight = mHeight / getStateCount();
		
		int positionY = (int) (dialStartY + distance);
		Log.d(TAG, "onDialMove(positionY: " + positionY + ")");
		mRectDestDial = new Rect(0, positionY, smallWidth, positionY + smallHeight);
	}
	
	/**
	 * @return the onSwitchStateChangedListener
	 */
	public OnSwitchStateChangedListener getOnSwitchStateChangedListener() {
		return mOnSwitchStateChangedListener;
	}
	/**
	 * @param onSwitchStateChangedListener the onSwitchStateChangedListener to set
	 */
	public void setOnSwitchStateChangedListener(
			OnSwitchStateChangedListener onSwitchStateChangedListener) {
		mOnSwitchStateChangedListener = onSwitchStateChangedListener;
	}

	private OnSwitchStateChangedListener mOnSwitchStateChangedListener;
	
	public static interface OnSwitchStateChangedListener {
		public void onStateChanged(int state, boolean fromUser);
	}
}
